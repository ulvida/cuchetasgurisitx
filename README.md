# Cuchetas gurisitx

Una cama de dos cuchetas que no require ni tornillos, ni clavos ni cola. Se requieren tablones en principio de 2 pulgadas de espesor, el diseño conserva los bordes rústicos de un lado del tablón. Los encastres se inspiran de los planos inclinados de la cola milan, particularmente eficientes para soportar esfuerzos transversales cahóticos, como los que pueden ejercer dos o tres niñxs jugando en la cama superior. 

Des lits supperposés en bois, qui n3e requièrent ni vis, ni clus ni colle. Les assemblages s'inspirent des plans inclinés des queues d'arronde, particulièrment adaptés pour supporter des efforts transversaux cahotiques, comme ceux que peuvent générer deux ou trois enfants jouant sur le lit supérieur.
